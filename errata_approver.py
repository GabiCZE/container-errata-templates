# ===============================================
# File: errata_approver.py 
# Author: Gabriela Necasova 
# Email: gnecasov@redhat.com
# Date: June 2023
# Description: This script replaces the Container Errata documentation texts in a given batch 
# with the template documentation texts. 
# In documentation process, we modify these fields: Synopsis, Topic, Problem Description, Solution and References.
# ===============================================

import argparse
import json
import logging
import os
import re
import requests
import sys

from errata_tool import ErrataConnector, Erratum
from requests_kerberos import HTTPKerberosAuth
   

# Install the Red Hat CA cert like this if you don't have it already:
#
#   cd /etc/pki/tls/certs
#   curl --insecure -o redhat-cacert.crt http://password.corp.redhat.com/cacert.crt
#   echo '59aa5919eccaf4cbf77dbc9ede454a65a9116d85d35447af0c2de2f62123042e redhat-cacert.crt' | sha256sum --check
#   ln -s redhat-cacert.crt `openssl x509 -hash -noout -in redhat-cacert.crt`.0
#   cat redhat-cacert.crt >> ca-bundle.crt
#
# Source: https://mojo.redhat.com/docs/DOC-926093
#
# (Or you can set verify=False below to skip the cert verification).
#
CA_CERTS = '/etc/pki/tls/certs/ca-bundle.crt'

BASE_URL = 'https://errata.devel.redhat.com/'
ADVISORY_URL = BASE_URL + '/api/v1/erratum/{0}'
BATCH_URL = BASE_URL + 'api/v1/batches/{0}'
GIT_IMAGE_URL = 'https://pkgs.devel.redhat.com/cgit/containers/'
SHOW_ADVISORY_URL = 'https://errata.devel.redhat.com/docs/show/'

# FIXME: In future, there might be more directories based on the product name (e.g. RHEL). 
# I guess the product name might be extracted from the batch name.
ERRATA_REPO_DIR_NAME = "RHEL/Containers"

# NOTE: Currently not used (loaded in the get_appstream_errata_mapping_json() function)
APPSTREAM_IMAGES = "appstream_images.json"


log = logging.getLogger()

def set_logger(level: str):
    formatter = logging.Formatter('%(levelname)s: %(message)s')
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    log.addHandler(console_handler)

    log_mode = logging.ERROR  # default level
    if level == 'debug':
        log_mode = logging.DEBUG
    elif level == 'info':
        log_mode = logging.INFO
    elif level == 'warning':
        log_mode = logging.WARNING

    log.setLevel(log_mode)


def print_errata(errata: Erratum):
    log.debug(f"""\n{'='*50}
    ADVISORY ID: {errata.errata_id}
    ADVISORY URL: {errata.url()}
    SYNOPSIS: {errata.synopsis}
    TOPIC: {errata.topic}
    DESCRIPTION: {errata.description}
    SOLUTION: {errata.solution}
    {'-'*50}\n""")


def get_batch_info(batch_id: int) -> dict:
    # DOC: https://errata.devel.redhat.com/documentation/developer-guide/api-http-api.html#batches
    return requests.get(BATCH_URL.format(batch_id), auth=HTTPKerberosAuth(), verify=CA_CERTS).json()


def put_advisory_info(errata_id: int, data: dict) -> int:
    # DOC: https://errata.devel.redhat.com/documentation/developer-guide/api-http-api.html#advisories
    return requests.put(ADVISORY_URL.format(errata_id), data=data, auth=HTTPKerberosAuth(), verify=CA_CERTS)


def get_advisory_info(errata_id: int) -> dict:
    # DOC: https://errata.devel.redhat.com/documentation/developer-guide/api-http-api.html#advisories
    return requests.get(ADVISORY_URL.format(errata_id), auth=HTTPKerberosAuth(), verify=CA_CERTS).json()
   

def get_template_path(e: Erratum) -> (str, str):
    # The doc template name can be extracted from Builds of a given errata     
    
    erratum_builds = e.errata_builds
    key = list(erratum_builds.keys())[0] # get the first (and only) key from the dictionary
    template_name = erratum_builds[key][0]

    gcc_toolset_version = ""
 
    match = re.search(r"(\S*container)\b", template_name)
    if match:
        template_name = match.group(1)
    else:
        log.warning("No template found!\n")   

    # for gcc-toolse-X-toolchain we extract the version (e.g. 12)
    if "gcc-toolset" in template_name:
        # Example: from "gcc-toolset-12-toolchain-container-12-73" we get "gcc-toolset-12-toolchain-container"
        template_name = re.sub(r'container[-\d]+', 'container', template_name)   
        # find the version (e.g. 12)
        gcc_toolset_version = re.findall(r'\d+', template_name)[0]
    
    # remove digits from template
    # reason: we use unify templates for all RHEL versions
    template_name = re.sub(r'\d+', '', template_name)
    
     # --------------------------------------------
    # NEW: regular expression (also catches Bug Fixes)
    # normal release build names: podman-container-9.3-8
    # Bug Fix build names: firefox-flatpak-el9-9030020230825103938.9
    template_name = re.sub(r'(.+?)-\d+\..+', '', template_name)

    if "container" not in template_name:
        template_name = template_name + "-container"
    # --------------------------------------------

    # if template contains version enclosed in '-', e.g. 'gcc-toolset-12-toolchain
    # then the resulting template name would be 'gcc-toolset--toolchain' and it is not a valid template name
    template_name = template_name.replace('--', '-')
    
    template_name = template_name + ".json"
    
    template_path = os.path.join(os.getcwd(), ERRATA_REPO_DIR_NAME, template_name)

    return template_name, template_path, gcc_toolset_version


def get_advisories_from_batch(batch_id: int) -> (list, str):
    # get info about batch 
    batch_info = get_batch_info(batch_id)

    # get batch release name (e.g.: RHEL-8.8.0.GA)
    batch_name = batch_info["data"]["relationships"]["release"]["name"]

    # extract the advisory IDs
    advisory_ids = [e["id"] for e in batch_info["data"]["relationships"]["errata"]]

    return advisory_ids, batch_name

def get_RHEL_version(e: Erratum) -> list:
    key = list(e.errata_builds.keys())[0]  # get the first (and only) key from the dictionary
    RHEL_version = re.findall(r'\d+', key)

    # convert the matched numbers to integers
    RHEL_version = [int(number) for number in RHEL_version]

    return RHEL_version    


def get_appstream_errata_mapping_json(e: Erratum, RHEL_version: list) -> list:
    # Get namespace-image mapping for Application Stream errata - from JSON
    # NOTE: Not used now, instead we use the get_appstream_errata_mapping_pkgs() 
    
    multi_images_list = []

    f = open(APPSTREAM_IMAGES, "r")
    appstream_images_dict = json.load(f)

    advisory_build_key = list(e.errata_builds.keys())[0]
    for build in e.errata_builds[advisory_build_key]:
        image_name = build.split('-container')[0]
        imageFound = False

        for key in appstream_images_dict.keys():
            if image_name == key:
                multi_images_list.append(appstream_images_dict[key] + RHEL_version + '/' + image_name)
                imageFound = True

        if not imageFound:
            log.warning(f"No entry found for image: {image_name}!")
            # FIXME: if not found - error and exit?

    log.info(f"MULTI IMAGE ERRATA LIST: {multi_images_list}")
    return multi_images_list


def get_appstream_errata_mapping_pkgs(e: Erratum, RHEL_version: list) -> list:
    # Get namespace-image mapping for Application Stream errata - from pkgs.devel.redhat.com
    
    multi_images_list = []
    
    advisory_build_key = list(e.errata_builds.keys())[0]
    
    parts = advisory_build_key.split(".")

    # join the first three parts with "."
    advisory_version = ".".join(parts[:3])

    for build in e.errata_builds[advisory_build_key]:
        image_name = build.split('-container')[0]

        # For example: https://pkgs.devel.redhat.com/cgit/containers/postgresql-13/tree/Dockerfile?h=rhel-9.3.0
        # Plain format: https://pkgs.devel.redhat.com/cgit/containers/postgresql-13/plain/Dockerfile?h=rhel-9.3.0
        image_url = f'{GIT_IMAGE_URL}{image_name}/plain/Dockerfile?h={advisory_version.lower()}'
        response = requests.get(image_url)

        # 1) remove the first line with the FROM instruction
        lines = response.text.split('\n')

        # filter out lines that start with "FROM"
        filtered_lines = [line for line in lines if not line.startswith('FROM')]

        # join the filtered lines back into a single string
        result_text = '\n'.join(filtered_lines)  

        # 2) search for the image namespace (ubi or rhel)
        # Note: RHEL_version[0] is the major version
        rhel_namespace = "rhel" + str(RHEL_version[0]) + '/'
        ubi_namespace = "ubi" + str(RHEL_version[0]) + '/'

        if rhel_namespace in result_text:
            multi_images_list.append(rhel_namespace + image_name)
        elif ubi_namespace in result_text: 
            multi_images_list.append(ubi_namespace + image_name)
        else:
            log.warning("fNo entry found for image: {image_name}!")
            # FIXME: if not found - error and exit?
    
    return multi_images_list


def replace_template_placeholders(template: dict, template_name: str, RHEL_version: list, multi_images_list: list, gcc_toolset_version: str) -> dict:
    # Replaces placeholders (enclosed in "{...}") in the template with valid equivalents

    image_name = template_name.split('-container')[0]
    # Synopsis
    # If it is a new RHEL version, e.g. 10.0.0
    if RHEL_version[1] == 0 and RHEL_version[2] == 0:
        template["synopsis"] = template["synopsis"].replace("{NEW_OR_UPDATED}", "New")
    else:
        template["synopsis"] = template["synopsis"].replace("{NEW_OR_UPDATED}", "Updated")  

    template["synopsis"] = template["synopsis"].replace("{RHEL_MAJOR_VERSION}", str(RHEL_version[0]))
    template["synopsis"] = template["synopsis"].replace("{IMAGE}", image_name)
    template["synopsis"] = template["synopsis"].replace("{VERSION}", gcc_toolset_version)


    # Topic
    if RHEL_version[1] == 0 and RHEL_version[2] == 0:
        if "An {NEW_OR_UPDATED}" in template["topic"]:
            template["topic"] = template["topic"].replace("{NEW_OR_UPDATED}", "new")
        else:
            template["topic"] = template["topic"].replace("{NEW_OR_UPDATED}", "New")

    else:
        if "An {NEW_OR_UPDATED}" in template["topic"]:
            template["topic"] = template["topic"].replace("{NEW_OR_UPDATED}", "updated")  
        else:
            template["topic"] = template["topic"].replace("{NEW_OR_UPDATED}", "Updated") 

    template["topic"] = template["topic"].replace("{RHEL_MAJOR_VERSION}", str(RHEL_version[0]))
    template["topic"] = template["topic"].replace("{IMAGE}", image_name)
    template["topic"] = template["topic"].replace("{VERSION}", gcc_toolset_version)

    # Problem Description
    template["problem_description"] = template["problem_description"].replace("{RHEL_MAJOR_VERSION}", str(RHEL_version[0]))
 
    if RHEL_version[1] == 0 and RHEL_version[2] == 0:
        template["problem_description"] = template["problem_description"].replace("{UPDATES_OR_ADDS}", "adds")
    else:
        template["problem_description"] = template["problem_description"].replace("{UPDATES_OR_ADDS}", "updates")  

    template["problem_description"] = template["problem_description"].replace("{IMAGE}", image_name)
    template["problem_description"] = template["problem_description"].replace("{VERSION}", gcc_toolset_version)

    if len(multi_images_list) > 0:
        # replace {IMAGE_LIST} with the list of images  
        multi_images_desc = template["problem_description"].replace("{IMAGE_LIST}", "\n".join(multi_images_list))
        template["problem_description"] = multi_images_desc

    # Note: Solution and References sections do not contain any replaceable items

    return template


def get_args():
    parser = argparse.ArgumentParser(description="Replaces the Container Errata documentation texts with the template documentation texts.\n \
                                    In documentation process, we modify these fields: Synopsis, Topic, Problem Description, Solution and References.")
    
    # the first group group specifies mandatory mode arguments: --advisory or --batch
    mode_group = parser.add_mutually_exclusive_group(required=True)

    # the second group group specifies mandatory action arguments: --approve (-a), --update (-u) or --disapprove (-d)
    action_group = parser.add_mutually_exclusive_group(required=True)

    mode_group.add_argument("--batch", type=int, help="specify the batch ID") 
    mode_group.add_argument("--advisory", type=int, help="specify the advisory ID")
    action_group.add_argument("-a", "--approve", action="store_true", help="approve the advisory")
    action_group.add_argument("-d", "--disapprove", action="store_true", help="disapprove the advisory")
    action_group.add_argument("-u", "--update", action="store_true", help="update the advisory")
    parser.add_argument("-l", "--level", type=str, default="warning", help="set logging level: error, warning, info, debug")

    return parser.parse_args()


def load_template(template_path: str, template_name: str) -> dict:
    try:
        with open(template_path, "r") as file:
            template = json.load(file)
    except FileNotFoundError:
        log.warning(f"File not found! Using the general-container.json template.")
        template_path = template_path.replace(template_name, "general-container.json")
        with open(template_path, "r") as file:
            template = json.load(file)
    except (IOError, Exception) as e:
        log.error("An error occurred while opening the file: {e}")
        return None  
    return template 


def image_name_sort_key(item: str) -> list:
    # Custom sort for Application Stream errata
    return item.split('/')


def process_multi_image_advisory(e: Erratum, advisory_build_list: list, template_path: str, template_name: str, RHEL_version: list) -> (str, list):
    
    multi_images_list = []
    if len(advisory_build_list) > 1 and not os.path.exists(template_path):

        # .NET advisory
        if any("dotnet-" in element for element in advisory_build_list):
            template_path = template_path.replace(template_name, "dotnet-container.json")

        # FDO advisory
        elif any("fdo-" in element for element in advisory_build_list):
            template_path = template_path.replace(template_name, "fdo-container.json")    

        # Appstream advisory
        else:
            # previously mapping was based on JSON: multi_images_list = get_appstream_errata_mapping_json(e, RHEL_version)
            multi_images_list = get_appstream_errata_mapping_pkgs(e, RHEL_version)
            # sort the input list using the custom sorting key
            multi_images_list = sorted(multi_images_list, key=image_name_sort_key)
            log.info(f"MULTI IMAGE LIST PKGS: {multi_images_list}")
            log.info(f"MULTI IMAGE LIST LEN: {len(multi_images_list)}")
            log.info(f"BUILD LIST LEN: {len(advisory_build_list)}")
            template_path = template_path.replace(template_name, "appstream-container.json")

    return template_path, multi_images_list


def main():  

    args = get_args()

    set_logger(args.level)

    if args.batch:
        advisory_ids, batch_name = get_advisories_from_batch(args.batch)
        log.info(f"PROCESSING BATCH: {args.batch}")
        log.info(f"BATCH RELEASE NAME: {batch_name}")
        log.info(f"https://errata.devel.redhat.com/batches/{args.batch}")
        log.info(f"ADVISORY IDs:\n{advisory_ids}")
        log.info(f"NUMBER OF ADVISORIES IN A BATCH: {len(advisory_ids)}")
        
    if args.advisory:
        advisory_ids = [args.advisory]
        log.info(f"PROCESSING ADVISORY={args.advisory}")

    if args.approve:
        log.info(f"ACTION: Approve")
    elif args.disapprove:
        log.info(f"ACTION: Disapprove")

    log.info('='*50)
 
    # -------------------------
    # loop through the advisory list
    # NOTE: for only 1 advisory - use advisory_ids[:1]
    #log.info(f"SELECTED ADVISORY IDS: {select_advisories}")
    for id in advisory_ids:
        
        # load errata info
        e = Erratum(errata_id=id) #e.g. 108752
        #log.info(f"ERRATA BUILDS: {e.errata_builds}") # key + list in JSON format
        advisory_build_key = list(e.errata_builds.keys())[0]
        advisory_build_list = e.errata_builds[advisory_build_key]

        log.info(f"ADVISORY BUILD KEY: {advisory_build_key}") 
        log.info(f"ADVISORY BUILD LIST: {advisory_build_list}") 
        log.info(f"ADVISORY BUILD LENGTH: {len(advisory_build_list)}")

        RHEL_version = get_RHEL_version(e)
        log.info(f"RHEL VERSION: {RHEL_version[0]}.{RHEL_version[1]}.{RHEL_version[2]}")

        # DISAPPROVE:
        if args.disapprove: 
            pdata = {"advisory[doc_complete]": "0"}
            post_res = put_advisory_info(e.errata_id, pdata) # status code: 204 (No Content - no body)
            log.info(f"DISAPPROVE STATUS: {post_res.status_code}")
            log.info(f"SYNOPSIS: {e.synopsis}")
            log.info(f"{SHOW_ADVISORY_URL}{e.errata_id}")
            log.info('='*50)
            continue
     
        # SKIP ADVISORY IF APPROVED
        # check if errata is already approved
        # if yes - skip it and continue
        e_info = get_advisory_info(e.errata_id)
        #log.info(f"E_INFO: " ,list(e_info["errata"].keys())[0])
        key = list(e_info["errata"].keys())[0]

        # Advisory is already approved. If update flag is not set, we can skip it
        if e_info["errata"][key]["doc_complete"] == 1:
            if not args.update:
                log.info(f"SKIPPING: ALREADY APPROVED")
                log.info(f"SYNOPSIS: {e.synopsis}")
                log.info(f"{SHOW_ADVISORY_URL}{e.errata_id}")
                log.info('='*50)
                continue

        # ---------------------------                
        # APPROVE OR UPDATE:         
        # get errata doc template path
        template_name, template_path, gcc_toolset_version = get_template_path(e)

        # get the path for multi-image advisory, if it is Application Stream advisory, we also need a list of updated images
        template_path, multi_images_list_sorted = process_multi_image_advisory(e, advisory_build_list, template_path, template_name, RHEL_version)                   
        log.info(f"TEMPLATE PATH: {template_path}")

        template = load_template(template_path, template_name)
        if template is None:
            return

        template = replace_template_placeholders(template, template_name, RHEL_version, multi_images_list_sorted, gcc_toolset_version)
  
        # replace errata text with template using the ErrataConnector
        # reason: the e.update() method cannot edit the "References" field
        data = {
            "advisory[synopsis]": template["synopsis"],
            "advisory[topic]": template["topic"],
            "advisory[description]": template["problem_description"],
            "advisory[solution]": template["solution"],
            "advisory[reference]": template["references"],
        }

        # -------------------------
        # 1) send data to server
        r = ErrataConnector()._put(
            u=f"/api/v1/erratum/{e.errata_id}", data=data
        )
        ErrataConnector()._processResponse(r)

        # 2) set DocApprove=1
        pdata = {"advisory[doc_complete]": "1"}
        post_res = put_advisory_info(e.errata_id, pdata) # status code: 204 (No Content - no body)
        log.info(f"APPROVE STATUS: {post_res.status_code}")
        #log.info(e.__dict__) # print whole object 
        synopsis = template['synopsis'].replace('\n', '')
        log.info(f"SYNOPSIS: {synopsis}")
        log.info(f"{SHOW_ADVISORY_URL}{e.errata_id}")
        log.info('='*50)
    
    
if __name__ == "__main__":
    main()
