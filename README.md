# errata-approver

## Description

This script replaces the Container Errata documentation texts with the template documentation texts. 
In documentation process, we modify these fields: Synopsis, Topic, Problem Description, Solution and References.


## Installation

```console
pip install errata-tool requests-kerberos
```

- [errata-tool](https://pypi.org/project/errata-tool/): Python API to Red Hat's Errata Tool (version 1.31.0)
- [requests-kerberos](https://pypi.org/project/requests-kerberos/): Kerberos/GSSAPI authentication (version 0.14.0)


## Usage

```console
./errata_approver.py [-h] (--batch BATCH | --advisory ADVISORY) (-a | -d | -u) [-l LEVEL]

Options:
  -h, --help               show this help message and exit
  --batch BATCH            specify the batch ID
  --advisory ADVISORY      specify the advisory ID
  -a, --approve            approve the advisory
  -d, --disapprove         disapprove the advisory
  -u, --update             update the advisory
  -l LEVEL, --level LEVEL  set logging level: 'error', 'warning' (default), 'info', 'debug'
```

**Example**: You want to approve all advisories in the `https://errata.devel.redhat.com/batches/2171` batch. 
Note that the output is shortened. 
```console
$ python errata_approver.py --batch 2171 --approve -l 'info'

INFO: PROCESSING BATCH: 2171
INFO: BATCH RELEASE NAME: RHEL-9.3.0.GA
INFO: https://errata.devel.redhat.com/batches/2171
INFO: ADVISORY IDs:
[117714, 117719, 117764, 117772, 118896, 118897, 118898, 118899, 118900, 118901, 118902, 118903, 118904, 118905, 118906, 118907, 118927, 119486, 119673, 119713, 119714, 119715, 119716, 119717, 119718, 119786]
INFO: NUMBER OF ADVISORIES IN A BATCH: 26
INFO: ACTION: Approve
INFO: ==================================================
INFO: ADVISORY BUILD KEY: RHEL-9.3.0.GA
INFO: ADVISORY BUILD LIST: ['ubi9-container-9.3-1088']
INFO: ADVISORY BUILD LENGTH: 1
INFO: RHEL VERSION: 9.3.0
INFO: TEMPLATE PATH: /home/gnecasov/Documents/Repos/container-errata-templates/RHEL/Containers/ubi-container.json
INFO: APPROVE STATUS: 204
INFO: SYNOPSIS: Updated ubi9 container image
INFO: https://errata.devel.redhat.com/docs/show/117714
INFO: ==================================================
INFO: ADVISORY BUILD KEY: RHEL-9.3.0.GA
INFO: ADVISORY BUILD LIST: ['ubi9-minimal-container-9.3-1091']
INFO: ADVISORY BUILD LENGTH: 1
INFO: RHEL VERSION: 9.3.0
INFO: TEMPLATE PATH: /home/gnecasov/Documents/Repos/container-errata-templates/RHEL/Containers/ubi-minimal-container.json
INFO: APPROVE STATUS: 204
INFO: SYNOPSIS: Updated ubi9-minimal container image
INFO: https://errata.devel.redhat.com/docs/show/117719
INFO: ==================================================
...
```

## Developer Notes

JIRA tracker: https://issues.redhat.com/browse/RHELDOCS-16586

List of replaceable items in templates

```console
{NEW_OR_UPDATED}      = ['New' | 'new' | 'Updated' | 'updated'] # based on the RHEL version (X.Y.Z)
{RHEL_MAJOR_VERSION}  = ['8' | '9' | '10' | ...] 
{UPDATES_OR_ADDS}     = ['updates' | 'adds'] # based on the RHEL version (X.Y.Z)
{IMAGE}               = image name derived from Builds

# special case - Application Stream:
{IMAGE_LIST}          = list of images in the format: <namespace>/<image>

# special case - GCC Toolset Toolchain:
{VERSION}          = version (e.g. 12, 13, etc.), derived from Builds
```

Known issue:
If we have a new image during the "non-X" release (e.g. 9.0.0) there will be a problem with replacing {NEW_OR_UPDATED} placeholder with words "new" vs "updated".
For example, for RHEL 9.3, we have a new rhel9/squid: https://errata.devel.redhat.com/errata/details/118897

## Adding new templates

1. If a template folder for your product does not exist, create it. 
* For example, `RHEL/Containers`.  
2. Optional: You can copy the [general template](https://gitlab.com/GabiCZE/container-errata-templates/-/blob/main/RHEL/Containers/general-container.json) as your starting file.
3. Copy the general template and name it according to the name in Builds in a given Advisory. 
* For example, for Podman, it is `podman-container-{numbers}`: https://errata.devel.redhat.com/advisory/127552/builds. The script takes a substring without `{numbers}`, and searches for the filename `podman-container.json`.
4. Adjust your template as needed. Specify these fields: Synopsis, Topic, Problem Description, Solution, (References).  

## Support
Gabriela Nečasová, gnecasov@redhat.com, Slack: Gabi N. 


